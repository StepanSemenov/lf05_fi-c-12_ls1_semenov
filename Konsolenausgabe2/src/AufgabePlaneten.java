/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie d�rfen nicht die Namen der Variablen ver�ndern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Stepan Semenov >>
  */

public class AufgabePlaneten {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    int 	anzahlPlaneten =  8;
    
    // Anzahl der Sterne in unserer Milchstra�e
    long	anzahlSterne = 400000000000L;
    
    // Wie viele Einwohner hat Berlin?
    int    bewohnerBerlin = 3645000;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
    int   alterTage = 10585;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
    float   gewichtKilogramm =   150000;
    
    // Schreiben Sie auf, wie viele km� das gr��te Land er Erde hat?
    int   flaecheGroessteLand = 17130000;
    
    // Wie gro� ist das kleinste Land der Erde?
    
    double   flaecheKleinsteLand = 0.44;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
    
    System.out.println("Anzahl der Sterne im Sonnensystem: " + anzahlSterne);
    
    System.out.println("Anzahl der Einwohner in Berlin: " + bewohnerBerlin);
    
    System.out.println("Wie alt sind sie in Tage: " + alterTage);
    
    System.out.println("Wie schwer ist das schwerste Tier der Welt: " + gewichtKilogramm);

    System.out.println("Welche Fl�che hat das gr��te Land der Welt: " + flaecheGroessteLand);

    System.out.println("Welche Fl�che hat das kleinste Land der Welt: " + flaecheKleinsteLand);

    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}
 