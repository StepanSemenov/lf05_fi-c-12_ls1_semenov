
public class Aufgabe3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int age = 0;
		
		System.out.printf("%-12s %s %10s\n", "Fahrenheit", "|", "Celsius");
		System.out.printf("%22s\n", "_________________________");
		System.out.printf("%-12.2f %s %10.2f\n", -20.00, "|", -28.8889);
		System.out.printf("%-12.2f %s %10.2f\n", -10.00, "|", -23.3333);
		System.out.printf("%+-12d %s %10.2f\n", 0, "|", -17.7778);
		System.out.printf("%+-12.2f %s %10.2f\n", 20.00, "|", -6.6667);
		System.out.printf("%+-12.2f %s %10.2f\n", 30.00, "|", -1.1111);

	}
}
