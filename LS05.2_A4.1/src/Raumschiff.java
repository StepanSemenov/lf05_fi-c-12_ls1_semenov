import java.util.ArrayList;

public class Raumschiff {

	private String schiffsname;
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();

	/** 
	 * Allgemeiner Konstruktor
	 * @param
	 * */public Raumschiff() {
	}
/**
 * Vollparametisierter Konstruktor
 * @param schiffsname
 * @param photonentorpedoAnzahl
 * @param energieversorgungInProzent
 * @param schildeInProzent
 * @param huelleInProzent
 * @param lebenserhaltungssystemeInProzent
 * @param androidenAnzahl
 */
	public Raumschiff(String schiffsname, int photonentorpedoAnzahl, int energieversorgungInProzent,
			int schildeInProzent, int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl) {

		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
	}

	public String getSchiffsname() {
		return schiffsname;
	}

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}
/**
 * Ladung wird dem Ladungsverzeichnis hinzugef�gt
 * @param ladung
 */
	public void addLadung(Ladung ladung) {
		ladungsverzeichnis.add(ladung);
	}

	/**
	 * Hier werden Informationen in den Log gespeichert
	 */
	public void addEntryInBroadcastKommunikator(String log) {
		broadcastKommunikator.add(log);
	}

	/** 
	 * Informationen aus dem Log ausgeben
	 * */public void broadcastKommunikatorAusgeben() {
		System.out.println("Test");
	}

	/**
	 * In dieser Methode wird ein Photonentorpedo auf ein Ziel abgeschossen
	 * @param ziel
	 */
	 public void photonentorpedoSchiessen(Raumschiff ziel) {
		if (photonentorpedoAnzahl == 0) {
			nachrichtAnAlle("-=*Click*=- - Keine Munition vorhanden!");
		} else {
			photonentorpedoAnzahl = photonentorpedoAnzahl - 1;
			nachrichtAnAlle("Die '" + schiffsname + "' hat einen Photonentorpedo abgeschossen");
			treffer(ziel);
		}
	}

	 /**
	  * In dieser Methode wird die Phaserkanone auf ein Ziel abgeschossen
	  * @param ziel
	  */
	public void phaserkanoneSchiessen(Raumschiff ziel) {
		if (energieversorgungInProzent < 50) {
			nachrichtAnAlle("-=*Click*=- - Nicht genug Energie!");
		} else {
			energieversorgungInProzent = energieversorgungInProzent - 50;
			nachrichtAnAlle("Die '" + schiffsname + "' hat eine Phaserkanone abgeschossen");
			treffer(ziel);
		}
	}
/**
 * Diese Methode beschreibt die Logik dessen, was passiert, wenn ein Schiff getroffen wurde. Dabei wird festgelegt, bei welchen Schild- und Huellenzust�nden welche Werte ver�ndert werden.
 * @param gegnerSchiff
 */
	private void treffer(Raumschiff gegnerSchiff) {
		System.out.println("Die '" + gegnerSchiff.schiffsname + "' wurde getroffen!");
		if (gegnerSchiff.schildeInProzent > 50) {
			gegnerSchiff.schildeInProzent = gegnerSchiff.schildeInProzent - 50;
		} else if (gegnerSchiff.schildeInProzent <= 50) {
			gegnerSchiff.schildeInProzent = 0;
			if (gegnerSchiff.huelleInProzent > 50) {
				gegnerSchiff.huelleInProzent = gegnerSchiff.huelleInProzent - 50;
				gegnerSchiff.energieversorgungInProzent = gegnerSchiff.energieversorgungInProzent - 50;
			} else {
				gegnerSchiff.lebenserhaltungssystemeInProzent = 0;
				gegnerSchiff.huelleInProzent = 0;
				gegnerSchiff.energieversorgungInProzent = 0;
				System.out.println("Die H�lle und die Lebenserhaltungssystem wurden zerst�rt!");
			}
		}
	}

	/**
	 * Diese Methode sendet eine Nachricht an alle.
	 * @param message
	 */
	public void nachrichtAnAlle(String message) {
		System.out.println(message);
	}
	
	/**
	 * L�dt Torpedos in die Kanone
	 * @param anzahlTorpedos
	 */public void photonentorpedosLaden(int anzahlTorpedos) {
	}
/**
 * Repapiert Schild und H�lle
 * @param schutzschilde
 * @param energieversorgung
 * @param schiffshuelle
 * @param anzahlDroiden
 */
	public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle,
			int anzahlDroiden) {
	}
/**
 * Diese Methode gibt die Informationen und Zust�nde eines ausgew�hlten Schiffs aus.
 */public void zustandRaumschiff() {
		System.out.println("\nSchiffsname: " + this.schiffsname);
		System.out.println("Anzahl Photonentorpedos: " + this.photonentorpedoAnzahl);
		System.out.println("Energieversorgung in Prozent: " + this.energieversorgungInProzent);
		System.out.println("Schilde in Prozent : " + this.schildeInProzent);
		System.out.println("Huelle in Prozent: " + this.huelleInProzent);
		System.out.println("Lebenserhaltungssysteme in Prozent: " + this.lebenserhaltungssystemeInProzent);
		System.out.println("Anzahl Androiden : " + this.androidenAnzahl);
	}
/**
 * Gibt das Ladungsverzeichnis eines Schiffes aus.
 */public void ladungsverzeichnisAusgeben() {

		System.out.println("\nLadungsverzeichnis" + ladungsverzeichnis.toString());
	}

	public void ladungsverzeichnisAufraeumen() {
	}
}