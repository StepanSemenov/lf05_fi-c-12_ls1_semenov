
public class main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Raumschiff klingonen = new Raumschiff("IKS Hegh'ta", 1, 100, 100, 100, 100, 2);
		Raumschiff romulaner = new Raumschiff("IRW Khazara", 2, 100, 100, 100, 100, 2);
		Raumschiff vulkanier = new Raumschiff("Ni'var", 0, 80, 80, 50, 100, 5);
		
		Ladung ldg1 = new Ladung("Ferengi Schneckensaft", 200);
		Ladung ldg2 = new Ladung("Borg-Schrott", 5);
		Ladung ldg3 = new Ladung("Rote Materie", 2);
		Ladung ldg4 = new Ladung("Forschungssonde", 35);
		Ladung ldg5 = new Ladung("Bat'leth Klingonen Schwert", 200);
		Ladung ldg6 = new Ladung("Plasma-Waffe", 50);
		Ladung ldg7 = new Ladung("Photonentorpedo", 3);

		klingonen.addLadung(ldg1);
		klingonen.addLadung(ldg5);

		romulaner.addLadung(ldg2);
		romulaner.addLadung(ldg3);
		romulaner.addLadung(ldg6);

		vulkanier.addLadung(ldg4);
		vulkanier.addLadung(ldg7);

		klingonen.addEntryInBroadcastKommunikator("test");
		klingonen.broadcastKommunikatorAusgeben();

		System.out.println("\nDas Schiff der Klingonen, die " + klingonen.getSchiffsname()
				+ "schie�t auf das Schiff der Romulaner, der " + romulaner.getSchiffsname() + ".");
		klingonen.photonentorpedoSchiessen(romulaner);
		System.out.println("\nDas Schiff der Romulaner, die " + romulaner.getSchiffsname() + ", schie�t zur�ck.\n");
		romulaner.phaserkanoneSchiessen(klingonen);
		vulkanier.nachrichtAnAlle("\nDas Schiff der Vulkanier, die " + vulkanier.getSchiffsname()
				+ ", sendet eine Nachricht an alle: Gewalt ist nicht logisch!");
		System.out.println("\nDas Schiff der Klingonen, die " + klingonen.getSchiffsname()
				+ ", gibt ihren Status und ihr Ladungsverzeichnis aus:");
		klingonen.zustandRaumschiff();
		klingonen.ladungsverzeichnisAusgeben();
		System.out.println("\nDas Schiff der Klingonen, die " + klingonen.getSchiffsname()
				+ ", will zwei weitere Photonentorpedos abschie�en");
		klingonen.photonentorpedoSchiessen(romulaner);
		klingonen.photonentorpedoSchiessen(romulaner);
		System.out.println("\nAlle drei Schiffe geben ihren Status aus!");
		klingonen.zustandRaumschiff();
		klingonen.ladungsverzeichnisAusgeben();
		romulaner.zustandRaumschiff();
		romulaner.ladungsverzeichnisAusgeben();
		vulkanier.zustandRaumschiff();
		vulkanier.ladungsverzeichnisAusgeben();
		
		klingonen.addEntryInBroadcastKommunikator("test");
		klingonen.broadcastKommunikatorAusgeben();
	}

}
