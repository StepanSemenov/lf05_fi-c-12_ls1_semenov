//Aufgabe 2, ungerade Zahlen
public class Array2 {

	public static void main(String[] args) {

		int[] zahlen = new int[10];
		for (int i = 0; i < zahlen.length; i++) {
			zahlen[i] = i+(i+1);
			System.out.printf("Zeige mir %s\n", zahlen[i]);
		}
	}
}