import java.util.Scanner;

public class PcHaendler {
	
	
	public static String liesString()
	{
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Was m�chten Sie bestellen?");
		String artikelMethode = myScanner.next();
		return artikelMethode;
	}
	
	public static int liesInt()
	{
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Geben Sie die Anzahl ein:");
		int anzahlMethode = myScanner.nextInt();
		return anzahlMethode;
	}
	
	public static double liesDouble()
	{
		Scanner myScanner = new Scanner(System.in);
		double preisMethode = myScanner.nextDouble();
		return preisMethode;
	}
	
	public static double berechneGesamtnettopreis(int anzahl, double nettopreis)
	{
		double nettoGesamtPreisMethode = anzahl * nettopreis;
		return nettoGesamtPreisMethode;
	}
	
	public static double berechneGesamtBruttopreis(double nettogesamtpreis, double mwst)
	{
		double bruttoGesamtPreisMethode = nettogesamtpreis * (1 + mwst / 100);
		return bruttoGesamtPreisMethode;
	}
	
	public static void rechnungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, 
			   double mwst) 
	{
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		// Benutzereingaben lesen
		 
		
		String artikel = liesString();
		
		int anzahl = liesInt();
		
		System.out.println("Geben Sie den Nettopreis ein:");
		double preis = liesDouble();

		System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		double mwst = liesDouble();

		// Verarbeiten
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		double bruttogesamtpreis = berechneGesamtBruttopreis (nettogesamtpreis, mwst);
		

		// Ausgeben

		rechnungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);
	}

}