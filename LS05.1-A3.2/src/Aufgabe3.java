import java.util.Scanner;

public class Aufgabe3 
{
	public static double reihenschaltung(double r1, double r2)
	{
		
	double ersatzwider = r1 + r2;
	return ersatzwider;
	
	}
	
	public static void main(String[] args)
	{
	Scanner scn = new Scanner(System.in);
	
	System.out.printf("Geben Sie bitte den Widerstand R1 an: ");
	double widerstandR1 = scn.nextDouble();
	System.out.printf("Geben Sie bitte den Widerstand R2 an: ");
	double widerstandR2 = scn.nextDouble();
	double ersatzWiderstand = reihenschaltung (widerstandR1, widerstandR2);
	System.out.printf("Der Ersatzwiderstand betr�gt %.2f Volt", ersatzWiderstand);
	
	}
}
