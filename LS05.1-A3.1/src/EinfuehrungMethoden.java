import java.util.Scanner;

public class EinfuehrungMethoden {
	
	public static int getUserInput() 
	{
		Scanner scn = new Scanner(System.in);
		int zahl = scn.nextInt();
		return zahl;
	}
	
	public static int addiere(int zahl1, int zahl2)
	{
		int summe = zahl1 + zahl2;
		return summe;
	}
	
	public static void gebeSummeAus(int summe) 
	{
		System.out.printf("Die Summe ist %d", summe);
	}
	
	public static void main(String[] args) 
	{
		Scanner scn = new Scanner(System.in);
		System.out.println("Geben Sie bitte die erste Zahl ein");
		int zahl01 = getUserInput();

		System.out.println("Geben Sie bitte die zweite Zahl ein");
		int zahl02 = getUserInput();

		int summe = addiere(zahl01, zahl02);
		
		gebeSummeAus(summe);

	}
}