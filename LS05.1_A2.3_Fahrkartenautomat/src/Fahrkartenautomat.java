import java.util.Scanner;

class methodenFahrkarten {

	public static void main(String[] args) {
		int millisekunde = 500; // Wartezeit des Ticketautomaten

		while (true) {

			// Fahrkartenkosten und -anzahl
			// -----------
			double zuZahlenderBetrag = fahrkartenbestellungErfassen();

			// Geldeinwurf
			// -----------
			double eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);

			// Fahrscheinausgabe
			// -----------------
			warte(millisekunde);

			// R�ckgeldberechnung und -Ausgabe
			// -------------------------------
			rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);

			System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
					+ "Wir w�nschen Ihnen eine gute Fahrt.\n");
		}
	}

	public static double fahrkartenbestellungErfassen() {
		Scanner tastatur = new Scanner(System.in);
		int anzahlTicketsInArray = 0;
		double zuZahlenderEndbetrag = 0;
		double[] ticketPreis = { 0.0, 2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90 };
		String[] ticketName = { "placeholder", "Einzelfahrschein Regeltarif AB", "Einzelfahrschein Regeltarif BC",
				"Einzelfahrschein Regeltarif ABC", "Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC",
				"Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC",
				"Kleingruppen-Tageskarte Berlin ABC" };

		if (ticketPreis.length == ticketName.length) {
			anzahlTicketsInArray = ticketPreis.length - 1;
		} else {
			System.out.print("Fehler! Die Anzahl der Preise und der Tickets stimmt nicht �berein, bitte korrigieren!\n");
			System.exit(0);
		}

		System.out.print("W�hlen Sie ihre Wunschfahrkarte aus:\n");
		System.out.print("Auswahlnummer \t\t\tBezeichnung \t\tPreis\n");
		for (int i = 1; i <= anzahlTicketsInArray; i++) {
			System.out.printf("(%2s) %45s %8.2f EUR\n", i, ticketName[i], ticketPreis[i]);
		}
		System.out.print("Ihre Wahl: ");
		int ticketArt = tastatur.nextInt();
		System.out
				.printf("Sie haben das Ticket '%s' ausgew�hlt, welches %.2f� kostet.\n", ticketName[ticketArt],
				ticketPreis[ticketArt]);

		System.out.print("Wieviele Tickets m�chten sie kaufen: ");
		int anzahlTickets = tastatur.nextInt();
		if (anzahlTickets >= 1 && anzahlTickets <= 10) {
			zuZahlenderEndbetrag = ticketPreis[ticketArt] * anzahlTickets;
			return zuZahlenderEndbetrag;
		} 
		else {
			System.out.print("Sie d�rfen nur maximal 10 Tickets auf einmal kaufen! Es wird jetzt 1 Ticket bestellt!");
			zuZahlenderEndbetrag = ticketPreis[ticketArt] * 1;
			return zuZahlenderEndbetrag;

		}

	}

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		Scanner tastatur = new Scanner(System.in);

		double eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf("\nNoch zu zahlen: %.2f Euro", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print("\nWerfen Sie ihre M�nzen ein (5ct, 10ct, 20ct, 50ct, 1�, 2�): ");
			double eingeworfeneM�nze = tastatur.nextDouble();
			if(eingeworfeneM�nze == 2.00 || eingeworfeneM�nze == 1.00 || eingeworfeneM�nze == 0.50 || eingeworfeneM�nze == 0.20 || eingeworfeneM�nze == 0.10 || eingeworfeneM�nze == 0.05) {
			eingezahlterGesamtbetrag += eingeworfeneM�nze;
			} else {
				System.out.print("\nUng�tlige Geldeingabe");
			}
		}
		return eingezahlterGesamtbetrag;
	}

	public static void warte(int millisekunde) {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(millisekunde);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}

	public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		double r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;

		if (r�ckgabebetrag > 0.0) {
			r�ckgabebetrag = Math.round(r�ckgabebetrag * 100) / 100.0;
			System.out.printf("Der R�ckgabebetrag in H�he von %.2f Euro ", r�ckgabebetrag);
			System.out.println("wird in folgenden M�nzen ausgezahlt:");
			
			while (r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
			{
				int muenzWert = 2;
				System.out.println("   * * *   ");
				System.out.println(" *       *");
				System.out.printf("\n*    %s    *", muenzWert);
				System.out.println("*   Euro  *");
				System.out.println(" *       *");
				System.out.println("   * * *   ");
				r�ckgabebetrag -= 2.0;
			}
			while (r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
			{
				int muenzWert = 1;
				System.out.println("   * * *   ");
				System.out.println(" *       *");
				System.out.printf("*    %s    *\n", muenzWert);
				System.out.println("*   Euro  *");
				System.out.println(" *       *");
				System.out.println("   * * *   ");
				r�ckgabebetrag -= 1.0;
			}
			while (r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
			{
				System.out.println("   * * *   ");
				System.out.println(" *       *");
				System.out.println("*    50   *");
				System.out.println("*   Cent  *");
				System.out.println(" *       *");
				System.out.println("   * * *   ");
				r�ckgabebetrag -= 0.5;
			}
			while (r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
			{
				System.out.println("   * * *   ");
				System.out.println(" *       *");
				System.out.println("*    20   *");
				System.out.println("*   Cent  *");
				System.out.println(" *       *");
				System.out.println("   * * *   ");
				r�ckgabebetrag -= 0.2;
			}
			while (r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
			{
				System.out.println("   * * *   ");
				System.out.println(" *       *");
				System.out.println("*    10   *");
				System.out.println("*   Cent  *");
				System.out.println(" *       *");
				System.out.println("   * * *   ");
				r�ckgabebetrag -= 0.1;
			}
			while (r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
			{
				System.out.println("   * * *   ");
				System.out.println(" *       *");
				System.out.println("*    5    *");
				System.out.println("*   Cent  *");
				System.out.println(" *       *");
				System.out.println("   * * *   ");
				r�ckgabebetrag -= 0.05;
			}
			
		}
	}
}