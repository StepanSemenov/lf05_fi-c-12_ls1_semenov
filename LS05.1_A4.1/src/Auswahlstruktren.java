import java.util.Scanner;

public class Auswahlstruktren {

	public static void main(String[] args) 
	{
		Scanner scn = new Scanner(System.in);
		System.out.println("Wenn Sie zu wenige Dinge im K�hlschrank haben, dann sollten sie einkaufen gehen");
		System.out.println("Wieviele Dinge haben Sie im K�hlschrank?: ");
		int anzahlDingeImKuehlschrank = scn.nextInt();
		System.out.println("Wie viele Dinge sollen im K�hlschrank mindestens sein?");
		int mindestanzahlDinge = scn.nextInt();

		if (anzahlDingeImKuehlschrank < mindestanzahlDinge)
		{
			System.out.println("Gehen Sie bitte etwas einkaufen!");
		}
		else if (anzahlDingeImKuehlschrank > mindestanzahlDinge)
		{
			System.out.println("Sie m�ssen noch nicht einkaufen gehen!");
		}
		else if (anzahlDingeImKuehlschrank == mindestanzahlDinge)
		{
			System.out.println("Sie haben genau so viele Sachen im K�hlschrank wie Sie mindestens brauchen. Es ist okay, aber ich w�rde trotzdem einkaufen gehen.");
		}
		
	}
}