public class Aufgabe4 {

	public static void main(String[] args) {
		
//a)		
		System.out.println("Dieses Programm verwendet For-Schleifen, um Zahlenreihen auszugeben!\n");
		System.out.println("Schleife 1 - 99 zu 0, 3er-Schritte");
		for (int i = 99; i >= 0; i = i - 3 ) 
		{
			System.out.printf("%s, ", i);
		}
//b)		
		System.out.println("\nSchleife 2 - 1 zu 400, 3er-Schritte");
		int wert = 0;
		int erhoehung = -1;
		while (wert < 400)
		{
			erhoehung = erhoehung + 2;
			wert = wert + erhoehung;
			System.out.printf("%s, ", wert);
		}
//c)		
		System.out.println("\nSchleife 3 - 2 zu 102, 4er-Schritte");
		for (int y = 2; y <= 102; y = y + 4 ) 
		{
			System.out.printf("%s, ", y);
		}
//d)
		System.out.println("\nSchleife 4 - 4 zu 1024, Schritte");
		int wertD = 0;
		int erhoehungD = -4;
		while (wertD < 1024)
		{
			erhoehungD = erhoehungD + 8;
			wertD = wertD + erhoehungD;
			System.out.printf("%s, ", wertD);
		}
//e
		System.out.println("\nSchleife 5 - 2 zu 32768, Verdoppelung");
		for (int z = 2; z <= 32768; z = z *2 ) 
		{
			System.out.printf("%s, ", z);
		}
	}

}
